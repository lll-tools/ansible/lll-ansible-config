# Install

-   Git clone this project:  
    
    ```shell
    git clone https://gitlab.com/lll-tools/ansible/lll-ansible-config.git
    cd lll-ansible-config
    ```
-   Create and activate a Python virtual environment ([venv](https://docs.python.org/3.8/library/venv.html)):  
    
    ```shell
    python3 -m venv venv
    source ./venv/bin/activate
    ```
-   Install **ansible** using `pip`:  
    
    ```shell
    python3 -m pip install ansible
    ```
-   Test the local playbook and deactivate the venv:  
    
    ```shell
    ansible-playbook -l local playbook_local.yml
    deactivate
    ```


# Config

-   `ansible.cfg`  
    Define default inventory:  
    
        inventory      = ./hosts.yml


## Inventory

<https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html>  

-   `hosts.ini`
-   `hosts.yml`
-   `inventory` directory


## Playbook

<https://docs.ansible.com/ansible/latest/user_guide/playbooks_intro.html>  

-   `playbook_local.yml`
-   `playbook_basic.yml`


# Doc


## Modules

<https://docs.ansible.com/ansible/latest/collections/index_module.html>  


## Inventory

<https://docs.ansible.com/ansible/latest/plugins/inventory.html#inventory-plugins>  

```shell
ansible-doc -t inventory -l
```


## Connection

<https://docs.ansible.com/ansible/latest/user_guide/connection_details.html>  

```shell
ansible-doc -t connection -l
```


## Galaxy

<https://galaxy.ansible.com/ansible/netcommon>  


# Vars precedence

<https://docs.ansible.com/ansible/latest/user_guide/playbooks_variables.html#define-variables-in-inventory>  

-   command line values (for example, -u my\_user, these are not variables)
-   role defaults (defined in role/defaults/main.yml) 1
-   inventory file or script group vars 2
-   inventory group\_vars/all 3
-   playbook group\_vars/all 3
-   inventory group\_vars/\* 3
-   playbook group\_vars/\* 3
-   inventory file or script host vars 2
-   inventory host\_vars/\* 3
-   playbook host\_vars/\* 3
-   host facts / cached set\_facts 4
-   play vars
-   play vars\_prompt
-   play vars\_files
-   role vars (defined in role/vars/main.yml)
-   block vars (only for tasks in block)
-   task vars (only for the task)
-   include\_vars
-   set\_facts / registered vars
-   role (and include\_role) params
-   include params
-   extra vars (for example, -e "user=my\_user")(always win precedence)


# Example commands

-   Ping devices and gather facts:  
    
    ```shell
    ansible all -m ping
    ansible all -m ansible.builtin.setup
    ```
-   Run commands  
    
    ```shell
    ansible all -m shell -a "uptime"
    ansible all -m command -a "uptime" --become -K
    ansible all --list-hosts
    ansible-playbook release.yml --extra-vars "version=1.23.45 other_variable=foo"
    ansible-playbook arcade.yml --extra-vars '{"pacman":"mrs","ghosts":["inky","pinky","clyde","sue"]}'
    ansible-playbook release.yml --extra-vars "@some_file.json"
    
    ```
-   Gather facts Vyos devices:  
    
    ```shell
    ansible all \
    	-i vyos.example.net, \
    	-c ansible.netcommon.network_cli \
    	-u my_vyos_user \
    	-k \
    	-m vyos.vyos.vyos_facts \
    	-e ansible_network_os=vyos.vyos.vyos
    ```