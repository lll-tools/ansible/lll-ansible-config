Luis Lain


1 Install
=========

  - Git clone this project:
    ,----
    | git clone https://gitlab.com/lll-tools/ansible/lll-ansible-config.git
    | cd lll-ansible-config
    `----
  - Create and activate a Python virtual environment ([venv]):
    ,----
    | python3 -m venv venv
    | source ./venv/bin/activate
    `----
  - Install *ansible* using `pip':
    ,----
    | python3 -m pip install ansible
    `----
  - Test the local playbook and deactivate the venv:
    ,----
    | ansible-playbook -l local playbook_local.yml
    | deactivate
    `----


[venv] https://docs.python.org/3.8/library/venv.html


2 Config
========

  - `ansible.cfg'
    Define default inventory:
    ,----
    | inventory      = ./hosts.yml
    `----


2.1 Inventory
~~~~~~~~~~~~~

  [https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html]
  - `hosts.ini'
  - `hosts.yml'
  - `inventory' directory


2.2 Playbook
~~~~~~~~~~~~

  [https://docs.ansible.com/ansible/latest/user_guide/playbooks_intro.html]
  - `playbook_local.yml'
  - `playbook_basic.yml'


3 Doc
=====

3.1 Modules
~~~~~~~~~~~

  [https://docs.ansible.com/ansible/latest/collections/index_module.html]


3.2 Inventory
~~~~~~~~~~~~~

  [https://docs.ansible.com/ansible/latest/plugins/inventory.html#inventory-plugins]
  ,----
  | ansible-doc -t inventory -l
  `----


3.3 Connection
~~~~~~~~~~~~~~

  [https://docs.ansible.com/ansible/latest/user_guide/connection_details.html]
  ,----
  | ansible-doc -t connection -l
  `----


3.4 Galaxy
~~~~~~~~~~

  [https://galaxy.ansible.com/ansible/netcommon]


4 Vars precedence
=================

  [https://docs.ansible.com/ansible/latest/user_guide/playbooks_variables.html#define-variables-in-inventory]
  - command line values (for example, -u my_user, these are not
    variables)
  - role defaults (defined in role/defaults/main.yml) 1
  - inventory file or script group vars 2
  - inventory group_vars/all 3
  - playbook group_vars/all 3
  - inventory group_vars/* 3
  - playbook group_vars/* 3
  - inventory file or script host vars 2
  - inventory host_vars/* 3
  - playbook host_vars/* 3
  - host facts / cached set_facts 4
  - play vars
  - play vars_prompt
  - play vars_files
  - role vars (defined in role/vars/main.yml)
  - block vars (only for tasks in block)
  - task vars (only for the task)
  - include_vars
  - set_facts / registered vars
  - role (and include_role) params
  - include params
  - extra vars (for example, -e "user=my_user")(always win precedence)


5 Example commands
==================

  - Ping devices and gather facts:
    ,----
    | ansible all -m ping
    | ansible all -m ansible.builtin.setup
    `----
  - Run commands
    ,----
    | ansible all -m shell -a "uptime"
    | ansible all -m command -a "uptime" --become -K
    | ansible all --list-hosts
    | ansible-playbook release.yml --extra-vars "version=1.23.45 other_variable=foo"
    | ansible-playbook arcade.yml --extra-vars '{"pacman":"mrs","ghosts":["inky","pinky","clyde","sue"]}'
    | ansible-playbook release.yml --extra-vars "@some_file.json"
    | 
    `----
  - Gather facts Vyos devices:
    ,----
    | ansible all \
    | 	-i vyos.example.net, \
    | 	-c ansible.netcommon.network_cli \
    | 	-u my_vyos_user \
    | 	-k \
    | 	-m vyos.vyos.vyos_facts \
    | 	-e ansible_network_os=vyos.vyos.vyos
    `----
